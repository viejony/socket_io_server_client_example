const io = require('socket.io')();
const port = 4023;

io.on('connection', client => {
  console.log('Connected client');
  client.on('data', (arg) => {
    console.log(arg);
  });
});


io.listen(port);

console.log('Socket listening in port ' + port);
