// Socket io client example

const io = require("socket.io-client");
const socket = io("http://www.server.com/");

socket.on("connect", () => {
    console.log("Client id = " + socket.id);
    socket.emit("data", 17);
});

socket.on("disconnect", () => {
    console.log("Client id = " + socket.id);
});

