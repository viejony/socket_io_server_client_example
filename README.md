# Socket.io server and client examples
A simple set of Nodejs packages that works as server and as a client, using *Socket.io*

## How to use

### Server side
1. Clone this repository in you server
2. Go to server package and install *npm* dependencies.
3. Edit the *index.js* file and change the port value.
4. Run the server with ```node index.js```

### Client side
1. Clone this repository in yor client system
2. Got to client package and install *npm* dependencies.
3. Edit *index.js* file and change the URL and port values to connect with the server
4. Run the client with ```node index.js```
